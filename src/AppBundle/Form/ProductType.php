<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\File\File;

class ProductType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** 
        *I tried to get automatically the file previously downloaded but that doesn't work like that
        */
        
        // $entity = $builder->getData();
        // $file = new File('/var/www/html/pearl/web/uploads/pictures/' . $entity->getPictureURL());
        // $entity->setPictureURL($file);

        $builder->add('name')->add('description')->add('pictureURL', FileType::class, array('data_class' => null))->add('price')->add('category');

    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Product'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_product';
    }


}
