<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Entity\Product;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Templating\PhpEngine;
use Symfony\Component\Templating\TemplateNameParser;
use Symfony\Component\Templating\Loader\FilesystemLoader;

class FluxController extends Controller
{
    /**
     * @Route("/feed/get/{slug}", name="feed_get_list", requirements={"slug"="\d+"})
     */
    public function list($slug)
    {
        /**
         * Render a feed according to the slug parameter, by default return an exception because there is no default feed
         * The feeds begin directly with 1, not 0 (normally), i found that more logic
         */
        switch ($slug) {
            case 1:
                $products = $this->getDoctrine()
                ->getRepository(Product::class)
                ->feedOne();
                break;
            case 2:
                $products = $this->getDoctrine()
                ->getRepository(Product::class)
                ->feedTwo();
                break;
            default: 
                throw $this->createNotFoundException('The feed does not exist');
        }

        return $this->render('feed/index.html.twig', array(
            'products' => $products,
        ));
    }

    /**
     * Matches /blog/*
     *
     * @Route("/blog/{slug}", name="blog_show")
     */
}