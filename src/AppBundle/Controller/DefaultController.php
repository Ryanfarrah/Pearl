<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use AppBundle\Entity\Product;

class DefaultController extends Controller
{
    /**
     * Home Controller
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {

        // $product = $this->getDoctrine()
        // ->getRepository(Product::class)
        // ->findAll();

        // var_dump($product->getCategory()->getName());

        // $serializer = $this->get('serializer');

        // $encoders = array(new XmlEncoder(), new JsonEncoder());
        // $normalizers = array(new ObjectNormalizer());

        // $serializer = new Serializer($normalizers, $encoders);

        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }
}
