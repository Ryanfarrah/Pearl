<?php 
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ProductRepository extends EntityRepository
{
    /**
     * This class is supposed to contain all method relating to the different feeds
     * The jointure property are named with camelCase instead of dots : ex : product.category.id = product.categoryId
     */

    public function feedOne() {
        return $this->getEntityManager()
        ->createQueryBuilder()
        ->select('p.name, p.description, p.pictureURL, p.price')
        ->from('AppBundle:Product', 'p')
        ->innerJoin('p.category','c')
        ->getQuery()
        ->getResult();
    }

    public function feedTwo() {
        return $this->getEntityManager()
        ->createQueryBuilder()
        ->select('p.name, p.pictureURL, p.price, c.name as categoryName')
        ->from('AppBundle:Product', 'p')
        ->innerJoin('p.category','c')
        ->getQuery()
        ->getResult();
    }
}