# Pearl
Voici le projet que vous m'aviez donné à réaliser et un petit compte-rendu de mes choix. 
Je n'ai pas trop eu le temps ces derniers jours ce pourquoi je vous le rends assez tard.

## Général
Le projet s'est bien déroulé, la partie de la gestion des données (tables, entities, fixtures etc..), la partie admin, le CRUD ont été plutôt faciles à réaliser.

Ce qui m'a donné du fil à retordre est le flux, je n'avais pas compris en fait l'objectif (j'essayais d'afficher le CSV dans le navigateur). 
Après que vous m'ayez répondu, j'ai préféré afficher le flux directement dans le navigateur (donc en HTML par Twig) car je trouve le procédé plus agréable pour l'utilisateur.

## CRUD

Pour le CRUD, j'ai utilisé un Bundle officiel offert par Symfony : https://symfony.com/doc/master/bundles/SensioGeneratorBundle/index.html.

Pour moi c'est une bonne utilisation, le Bundle n'est pas très gros et répond totalement aux besoins du CRUD (création, édition, visualisation, suppression). 

J'ai du juste ajouté manuellement le stockage du fichier image (pictureURL) quand on ajoute un produit ainsi que sa suppression quand on supprime un produit (en utilisant https://symfony.com/doc/current/components/filesystem.html)

## Flux
La partie la plus dure pour moi, le fait déjà que je n'avais pas compris mais la difficulté est resté (je me l'a suis imposé peut-être) pour que le minimum d'effort soit nécessaire pour ajouter un flux.

Je vais vous présenter ma logique ici :

Les flux sont disponibles par n'importe qui à partir de ce modèle : /feed/get/{slug} (où slug est obligatoirement un nombre, j'ai trouvé cela plus logique d'imposer un nombre)

Ensuite le $slug va être passé au Controller qui va faire un **switch**  en PHP et selon le numéro va appelé un requête spécifique. C'est un bon moyen de fournir des multitudes de flux (pour des dizaines et des dizaines de flux, je trouve que c'est propre, pour des milliers, là il faut surement trouver autre chose).

Les requêtes pour les flux sont tous stockées dans le **Repository** de l'entité **Product**.  C'est eux qui contiennent toute la logique pour afficher les champs demandés par les flux.

Ensuite il n'existe qu'un template Twig pour tous les flux, le template Twig vérifie que chaque donnée à afficher dans le template est transmise par le Controller, si oui il l'affiche, sinon il passe outre.

Je trouve que la logique employée est assez bonne pour créer des flux en l'espace de quelques minutes ; il faut rajouter une case switch pour un flux donnée, créer une nouvelle requête décrivant les champs à obtenir dans le **Repository** et le template s'occupe du reste.





